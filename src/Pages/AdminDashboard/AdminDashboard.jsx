import React from 'react'
import AdminDashboardContent from './AdminDashboardContent'
import SideBar from '../../Components/Sidebar/SideBar'

export default function AdminDashboard() {
  return (
    <div>
        <SideBar childComponent={<AdminDashboardContent />} ></SideBar>
    </div>
  )
}
