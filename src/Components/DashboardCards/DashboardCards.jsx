import React, { useState } from 'react'
import "./DashboardCards.css"
import { useEffect } from 'react';
import axios from 'axios';

export default function DashboardCards() {

  const [totalUsers , settotalUsers] = useState();


  useEffect(() => {
    axios.get('http://127.0.0.1:8000/api/get-usercount').then((response)=>{
      console.log(response.data);
      settotalUsers(response.data)
    }).catch((error)=>{
      console.log(error)
    })
  }, []);



  return (
    <div className="topCardstesting container">
    <div class="row gap-5">
    <div class="topCard col">
        <h4>Total Users</h4>
        <p>{totalUsers}</p>
           <button class="topCardButton">
			<span>Learn more</span>
			<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="none">
				<path d="M0 0h24v24H0V0z" fill="none" />
				<path d="M16.01 11H4v2h12.01v3L20 12l-3.99-4v3z" fill="currentColor" />
			</svg>
		</button>
    </div>

    <div class="topCard col">
        <h4>Total Nodes</h4>
        <p>10</p>
           <button class="topCardButton">
			<span>Learn more</span>
			<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="none">
				<path d="M0 0h24v24H0V0z" fill="none" />
				<path d="M16.01 11H4v2h12.01v3L20 12l-3.99-4v3z" fill="currentColor" />
			</svg>
		</button>
    </div>

    <div class="topCard col">
        <h4>Total Clusters</h4>
        <p>10</p>
           <button class="topCardButton">
			<span>Learn more</span>
			<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="none">
				<path d="M0 0h24v24H0V0z" fill="none" />
				<path d="M16.01 11H4v2h12.01v3L20 12l-3.99-4v3z" fill="currentColor" />
			</svg>
		</button>
    </div>


</div>
    </div>
  
  )
}
