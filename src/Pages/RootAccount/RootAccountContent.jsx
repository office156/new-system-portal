import React, { useState } from 'react'
import "./RootAccount.css"
import { useEffect } from 'react';
import axios from 'axios';

export default function RootAccountContent() {


    const [loginTime , setLoginTime] = useState();
    const [systemConfo , setSystemConfo] = useState([
        {
          hostname: "something",
          transient: "something",
          iconname: "something",
          chassis: "something",
          machineid: "something",
          bootid: "something",
          operatingsystem: "something",
          cpeosname: "something",
          kernel: "something",
          arch: "something",
        }
      ]);
    const [systemInfo , setSystemInfo] = useState([
        {
          hostname: "something",
          transient: "something",
          iconname: "something",
          chassis: "something",
          machineid: "something",
          bootid: "something",
          operatingsystem: "something",
          cpeosname: "something",
          kernel: "something",
          arch: "something",
        }
      ]);
    const [systime , setsystime] = useState();

    useEffect(() => {
        axios.get('http://127.0.0.1:8000/api/get-lastlogin').then((response)=>{
            console.log(response.data);
            setLoginTime(response.data)
          }).catch((error)=>{
            console.log(error)
          })

        axios.get('http://127.0.0.1:8000/api/get-systemConfiguration').then((response)=>{
            console.log(response.data);
            setSystemConfo(response.data)
          }).catch((error)=>{
            console.log(error)
          })
          axios.get('http://127.0.0.1:8000/api/get-systemInformation').then((response)=>{
            console.log(response.data);
            setSystemInfo(response.data)
          }).catch((error)=>{
            console.log(error)
          })
        axios.get('http://127.0.0.1:8000/api/get-systime').then((response)=>{
            console.log(response.data);
            setsystime(response.data)
          }).catch((error)=>{
            console.log(error)
          })
      },[]);
    

    return (

        <div className="RootAccountContent">
            <div className="RootAccountrowTop">
                <div class="container text-start">

                {/* <div class="grid gap-3">
  <div class="p-2 g-col-6">Grid item 1</div>
  <div class="p-2 g-col-6">Grid item 2</div>
  <div class="p-2 g-col-6">Grid item 3</div>
  <div class="p-2 g-col-6">Grid item 4</div>
</div> */}
                    <div class="row gap-3">
                        <div class="p-2 col border">
                            <h3 className=''>Health</h3>
                            <div className="serviceFailLine">
                            <i class="fa-solid fa-circle-exclamation"></i>
                                <span></span>
                                <span> service has failed</span>
                            </div>
                            <div className="serviceFailLine">
                            <i class="fa-solid fa-circle-check"></i>
                                <span></span>
                                <span> service is running ok</span>
                            </div>
                            <div className="securityUpdatesLine">
                            <i class="fa-solid fa-triangle-exclamation"></i>
                                <span></span>
                                <span> security updates available</span>
                            </div>
                            <div className="lastSuccessfulLoginLine">
                                <i></i>
                                <span>Last successful login : </span>
                                <span></span>
                                <p>{loginTime}</p>
                                <a href="">View login history</a>
                            </div>

                        </div>
                        <div class="p-2 col border">
                            <h3 className=''>Usage</h3>
                            <div className="systemInformationTable">
                                <table className='w-100'>
                                    <tr>
                                        <td>CPU</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Memory</td>
                                        <td></td>
                                    </tr>

                                </table>
                            </div>
                            <div className="">
                                <a href="">View hardware details</a>
                            </div>

                        </div>
                    </div>

                    <div class="row mt-4 gap-3">

                        <div class="col border py-3 px-3">
                            <h3 className=''>System information</h3>
                            <div className="systemInformationTable">
                                <table className='w-100'>
                                    <tr>
                                        <td>Family</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Manufacturer</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Product Name</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Serial No</td>
                                        <td></td>
                                    </tr>
                                    {/* <tr>
                                        <td>SKU No</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>UUID</td>
                                        <td></td>
                                    </tr> */}
                                    <tr>
                                        <td>Version</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Wakeup type</td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <div className="">
                                <a href="">View hardware details</a>
                            </div>
                        </div>

                        <div class="col border py-3 px-3">
                            <h3 className=''>Configuration</h3>
                            <div className="systemInformationTable">
                                <table className='w-100'>
                                    <tr>
                                        <td>Hostname</td>
                                        <td>{systemConfo[0]["hostname"]}</td>
                                        {/* <td>{systemConfo.length > 0 ? <span></span> : <></>}</td> */}
                                    </tr>
                                    <tr>
                                        <td>System time</td>
                                        <td>{systime}</td>
                                    </tr>
                                    <tr>
                                        <td>Domain</td>
                                        <td>{systemConfo[0]["hostname"].split('.').slice(1).join('.')}</td>
                                    </tr>
                                    <tr>
                                        <td>Operating System</td>
                                        <td>{systemConfo[0]["operatingsystem"]}</td>
                                    </tr>
                                    <tr>
                                        <td>Kernel</td>
                                        <td>{systemConfo[0]["kernel"]}</td>
                                    </tr>
                                    <tr>
                                        <td>Architecture</td>
                                        <td>{systemConfo[0]["arch"]}</td>
                                    </tr>
                                </table>
                            </div>
                            <div className="">
                                <a href="">View hardware details</a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    )
}
