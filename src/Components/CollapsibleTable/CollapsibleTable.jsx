import * as React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import TablePagination from '@mui/material/TablePagination';
import "./CollapsibleTable.css"

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);

  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {row.uid}
        </TableCell>
        <TableCell align="right">{row.gidNumber}</TableCell>
        <TableCell align="right">{row.homeDirectory}</TableCell>
        <TableCell align="right">{row.mail}</TableCell>
        <TableCell align="right">{row.mobile}</TableCell>
        <TableCell align="right">{row.displayName}</TableCell>
        {/* <TableCell align="right">{row.fat}</TableCell>
        <TableCell align="right">{row.carbs}</TableCell>
        <TableCell align="right">{row.protein}</TableCell> */}
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant="h6" gutterBottom component="div">
                Extra
              </Typography>
              <Table size="small" aria-label="purchases">

                <TableBody>
                 
                    <TableRow key={row.uid}>
                    <TableCell className = "font-weight-bold">Amount</TableCell>
                      <TableCell component="th" scope="row">
                        {row.amount}
                      </TableCell>
                    </TableRow>

                    <TableRow key={row.application}>
                    <TableCell className = "font-weight-bold">Application</TableCell>
                    <TableCell>{row.application}</TableCell>
                    </TableRow>

                    <TableRow key={row.cpuhours}>
                    <TableCell className = "font-weight-bold">CPU hours</TableCell>
                    <TableCell>{row.cpuhours}</TableCell>
                    </TableRow>

                    <TableRow key={row.department}>
                    <TableCell className = "font-weight-bold">Department</TableCell>
                    <TableCell>{row.department}</TableCell>
                    </TableRow>

                    <TableRow key={row.designation}>
                    <TableCell className = "font-weight-bold">Designation</TableCell>
                    <TableCell>{row.designation}</TableCell>
                    </TableRow>

                    <TableRow key={row.domain}>
                    <TableCell className = "font-weight-bold">Domain</TableCell>
                    <TableCell>{row.domain}</TableCell>
                    </TableRow>

                    <TableRow key={row.enddate}>
                    <TableCell className = "font-weight-bold">End date</TableCell>
                    <TableCell>{row.enddate}</TableCell>
                    </TableRow>
                    <TableRow key={row.funded}>
                    <TableCell className = "font-weight-bold">Funded</TableCell>
                    <TableCell>{row.funded}</TableCell>
                    </TableRow>
                    <TableRow key={row.gender}>
                    <TableCell className = "font-weight-bold">Gender</TableCell>
                    <TableCell>{row.gender}</TableCell>
                    </TableRow>
                    <TableRow key={row.gpuhours}>
                    <TableCell className = "font-weight-bold">GPU hours</TableCell>
                    <TableCell>{row.gpuhours}</TableCell>
                    </TableRow>
                    <TableRow key={row.institute}>
                    <TableCell className = "font-weight-bold">Institute</TableCell>
                    <TableCell>{row.institute}</TableCell>
                    </TableRow>
                    <TableRow key={row.pihod}>
                    <TableCell className = "font-weight-bold">PI-HOD</TableCell>
                    <TableCell>{row.pihod}</TableCell>
                    </TableRow>
                    
                    <TableRow key={row.projectname}>
                    <TableCell className = "font-weight-bold">Project Name</TableCell>
                    <TableCell>{row.projectname}</TableCell>
                    </TableRow>
                    
                    <TableRow key={row.startdate}>
                    <TableCell className = "font-weight-bold">Start date</TableCell>
                    <TableCell>{row.startdate}</TableCell>
                    </TableRow>
                    
                    <TableRow key={row.subdomain}>
                    <TableCell className = "font-weight-bold">Sub domain</TableCell>
                    <TableCell>{row.subdomain}</TableCell>
                    </TableRow>
                    
                      
                  
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}



export default function CollapsibleTable({columns , rows}) {

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
  
    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(+event.target.value);
      setPage(0);
    };

  return (
    <Paper>
    <TableContainer>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell align="right">UID</TableCell>
            <TableCell align="right">gid</TableCell>
            <TableCell align="right">Home directory</TableCell>
            <TableCell align="right">Email</TableCell>
            <TableCell align="right">Mobile No</TableCell>
            <TableCell align="right">Display name</TableCell>
            {/* <TableCell align="right"></TableCell> */}
            {/* <TableCell align="right">Fat&nbsp;(g)</TableCell>
            <TableCell align="right">Carbs&nbsp;(g)</TableCell>
            <TableCell align="right">Protein&nbsp;(g)</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {/* {rows.map((row) => (
            <Row key={row.uid} row={row} />
          ))} */}

                      {rows
                          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                          .map((row) => (
                            <Row key={row.uid} row={row} />
                          ))}
        </TableBody>
      </Table>

    </TableContainer>
    <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
}