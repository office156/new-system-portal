const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(
    '/api', // The path you want to proxy, e.g., /api/tickets.json
    createProxyMiddleware({
      target: 'http://10.208.35.206', // The target server
      changeOrigin: true, // Change the origin to match the target's origin
    })
  );
};