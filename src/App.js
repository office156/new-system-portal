import { BrowserRouter as Router, Switch, Route, Routes } from 'react-router-dom';
import LandingPage from './Pages/LandingPage/LandingPage';
import Login from './Pages/Login/Login';
import AdminDashboard from './Pages/AdminDashboard/AdminDashboard';
import RootAccount from './Pages/RootAccount/RootAccount';
import Test from './Pages/Test/Test';
import AddUser from './Pages/AdminAddUser/AddUser';
import NodeDefination from './Pages/NodeDefination/NodeDefination';
import ImageGroups from './Pages/ImageGroups/ImageGroups';
import WorkloadManager from './Pages/WorkloadManager/WorkloadManager';
import WorkloadManager2 from './Pages/WorkloadManager2/WorkloadManager2';
import NetworkPage from './Pages/Network/NetworkPage';
import ViewUserMain from './Pages/ViewUser/ViewUserMain';
import DeleteUser from './Pages/DeleteUser/DeleteUser';
import AccordianAddUser from './Pages/AddUser/AccordianAddUser';


function App() {
  return (
<Router>
    <div className="App">
      <Routes>
                 <Route exact path='/test' element={< Test />}></Route>

                 <Route exact path='/' element={< LandingPage />}></Route>
                 <Route exact path='/login' element={< Login />}></Route>
                 <Route exact path='/admin-dashboard' element={< AdminDashboard />}></Route>
                 <Route exact path='/root-account' element={< RootAccount />}></Route>
                 <Route exact path='/new-user' element={<><AddUser /></>}></Route>
                 <Route exact path='/view-user' element={<><ViewUserMain /></>}></Route>
                 <Route exact path='/delete-user' element={<><DeleteUser /></>}></Route>
                 <Route exact path='/add-user' element={<><AccordianAddUser /></>}></Route>
                 <Route exact path='/node-defination' element={<><NodeDefination /></>}></Route>                 
                 <Route exact path='/image-groups' element={<><ImageGroups /></>}></Route>
                 
                 <Route exact path='/network-groups' element={<><NetworkPage /></>}></Route>
                 
                 <Route exact path='/workload' element={<><WorkloadManager /></>}></Route>
                 <Route exact path='/workload2' element={<><WorkloadManager2 /></>}></Route>
                 
          </Routes>
    </div>
    </Router>
  );
}

export default App;
