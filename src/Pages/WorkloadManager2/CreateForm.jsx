import React from 'react'
import { useState } from 'react';
import { toast , ToastContainer } from 'react-toastify';
import axios from 'axios';

export default function CreateForm() {

  const initialFormData = {
    qname: '',
    enabled: '',
    started: '',
    dnodes: '',
    dwalltime: '',
    dqueue: '',
  };

  const [formData, setFormData] = useState(initialFormData);


  const handleChange =  (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(formData)
    if (formData.dqueue === '' || formData.qname === '' || formData.enabled === '' || formData.started === '' || formData.dnodes === '' || formData.dwalltime === '') {
      toast.error('Please select an option.');
      return;
    }
    console.log(formData)
    try {
      const response = await axios.post('http://127.0.0.1:8000/api/admin/pbs-create-queue', {
        formData
      });

    } catch (error) {
      console.error(error);
      toast.error(error)
    }
    handleReset();
  };

  const handleReset = () => {
    setFormData(initialFormData);
  };


  return (
    <div>
<form >
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Queue Name</label>
    <input type="text" class="form-control" id='exampleInputEmail1' name='qname' value={formData.qname}  onChange={handleChange} required></input>
    {/* <input type="text" name="name" onChange={handleChange} /> */}
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label me-2">Started : </label>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" required name="started"  value="true" checked={formData.started === 'true'}  onChange={handleChange}></input>
  <label class="form-check-label" for="inlineRadio1">True</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" required  name="started" value="false" checked={formData.started === 'false'}  onChange={handleChange}></input>
  <label class="form-check-label" for="inlineRadio2">False</label>
</div>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label me-2 ">Enabled :</label>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="enabled"   value="true" checked={formData.enabled === 'true'}  onChange={handleChange} ></input>
  <label class="form-check-label" for="enabledRadio1">True</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="enabled"   value="false" checked={formData.enabled === 'false'} onChange={handleChange} ></input>
  <label class="form-check-label" for="enabledRadio2">False</label>
</div>
  </div>
  <div class="mb-3">
    <label for="exampleInputDefaultNodes1" class="form-label">Default Nodes</label>
    <input type="text" class="form-control" id="exampleInputDefaultNodes1" name='dnodes' value={formData.dnodes}  onChange={handleChange} required></input>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Default walltime</label>
    <input type="text" class="form-control" id="exampleInputPassword1" name='dwalltime' value={formData.dwalltime}  onChange={handleChange} required></input>
  </div>
  <div class="mb-3">
    <label for="" class="form-label me-2">Default Queue :</label>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" required name="dqueue" value="true" checked={formData.dqueue === 'true'}  onChange={handleChange}></input>
  <label class="form-check-label" for="queueRadio1">True</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" required name="dqueue"  value="false" checked={formData.dqueue === 'false'}  onChange={handleChange}></input>
  <label class="form-check-label" for="queueRadio2">False</label>
</div>
  </div>
  <button onClick={handleSubmit} class="btn btn-primary">Submit</button>
  </form>

  <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>

    </div>
  )
}
