import React from 'react'
import DeleteUserContent from './DeleteUserContent'
import SideBar from '../../Components/Sidebar/SideBar'

export default function DeleteUser() {
  return (
    <div>
      <SideBar childComponent={<DeleteUserContent />} ></SideBar>
    </div>
  )
}
