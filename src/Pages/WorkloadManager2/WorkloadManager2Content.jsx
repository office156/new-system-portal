import React from 'react';
import { useState } from 'react';
import { toast, ToastContainer } from 'react-toastify';
import "./WorkloadManager.css";
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import EnhancedTable from './dummy';
import { useRef } from 'react';
import TableComponent from '../../Components/TableComponent/TableComponent';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import BatchJob from '../../Components/WorkloadCreateforms/BatchJob';
import Partition from '../../Components/WorkloadCreateforms/Partition';
import Reservation from '../../Components/WorkloadCreateforms/Reservation';

import WorkloadSearchTab from '../../Components/WorkloadSearchTab/WorkloadSearchTab';
import SideBar from '../../Components/Sidebar/SideBar';
import CreateForm from './CreateForm';
import BasicAccordion from './Accordian';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function WorkloadManager2Content() {

    const [isAddNodeVisible, setIsAddNodeVisible] = useState(false);
    const [isCustomNodeVisible, setIsCustomNodeVisible] = useState(false);
    const [isDeleteNodeVisible, setIsDeleteNodeVisible] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const fileInputRef = useRef(null);
    const [NodeInfo , setNodeInfo] = useState([]);
    const [squeue , setsqueue] = useState([]);
    const [jobsinfo , setjobsinfo] = useState([]);
    const [sinfo , setsinfo] = useState([]);
    const [saccount , setsaccount] = useState([]);

    const [groups , setgroups] = useState([]);
    const [data , setdata] = useState([]);
    const [node , setNode] = useState();
    const [job, setJob] = useState();
    
    const [selectedFile, setSelectedFile] = useState("");

    const [showqueue , setshowqueue] = useState(false);
    const [showsaccount , setshowsaccount] = useState(false);

    const columns = [
      { field: 'jobId', headerName: 'Job Id' },
      { field: 'name', headerName: 'User name' },
      {
        field: 'queue',
        headerName: 'Queue'
      },
      { field: 'jobname', headerName: 'Job name'},
      { field: 'sessionid', headerName: 'Session Id'},
      { field: 'nds', headerName: 'NDS'},
      { field: 'tsk', headerName: 'TSK'},
      { field: 'memory', headerName: 'Reqd Memory'},
      { field: 'time', headerName: 'Reqd Time'},
      { field: 'state', headerName: 'S'},
      { field: 'left_time', headerName: 'Elap time'},
    ];

    const saccountcolumns = [
      { field: 'account', headerName: 'Account' },
      { field: 'descr', headerName: 'Descr' },
      {
        field: 'org',
        headerName: 'Org'
      }
    ];

    const sinfocolumns = [
      { field: 'reason', headerName: 'Reason' },
      { field: 'user', headerName: 'User' },
      { field: 'timestamp', headerName: 'Timestamp' },
      {
        field: 'nodelist',
        headerName: 'Nodelist'
      }
    ];

    const jobsinfocolumns = [
      { field: 'user', headerName: 'User' },
      { field: 'jobid', headerName: 'jobid' },
      { field: 'jobname', headerName: 'jobname' },
      {
        field: 'partition',
        headerName: 'partition'
      },
      { field: 'state', headerName: 'state' },
      { field: 'timelimit', headerName: 'timelimit' },
      { field: 'start', headerName: 'start' },
      { field: 'end', headerName: 'end' },
      { field: 'elapsed', headerName: 'elapsed' },
      // { field: 'maxrss', headerName: 'maxrss' },
      // { field: 'maxvmsize', headerName: 'maxvmsize' },
      // { field: 'nnodes', headerName: 'nnodes' },
      { field: 'ncpus', headerName: 'ncpus' },
      { field: 'nodelist', headerName: 'nodelist' },
    ];

    
// const rows = [
//   [1, 'Snow','Jon',35 ],
//   [2, 'Lannister','Cersei',42 ],
//   [3, 'Lannister','Jaime',45 ],
//   [4, 'Stark','Arya',16 ],
//   [5, 'Targaryen','Daenerys',null ],
//   [6, 'Melisandre',null,150 ],
//   [7, 'Clifford','Ferrara',44 ],
//   [8, 'Frances','Rossini',36 ],
//   [9, 'Roxie','Harvey',65 ]
// ];

    const fetchSqueue = () =>{
      axios.get('http://127.0.0.1:8000/api/get-qstat-squeue').then((response) =>{
        console.log(response)
        setsqueue(response.data)
      }).catch((error) =>{
        console.log(error)
      })
    }

    const fetchjobsInfo = () =>{
      axios.get('http://127.0.0.1:8000/api/get-jobsinfo').then((response) =>{
        console.log(response)
        setjobsinfo(response.data)
      }).catch((error) =>{
        console.log(error)
      })
    }

    const fetchSinfo = () =>{
      axios.get('http://127.0.0.1:8000/api/get-sinfo').then((response) =>{
        console.log(response)
        setsinfo(response.data)
      }).catch((error) =>{
        console.log(error)
      })
    }

    // const fetchSaccount = () =>{
    //   // qstat -u 
    //   axios.get('http://127.0.0.1:8000/api/get-saccount').then((response) =>{
    //     console.log(response)
    //     setsaccount(response.data)
    //   }).catch((error) =>{
    //     console.log(error)
    //   })
    // }
    



    const searchNode = () => {
      console.log(node)
      const url = 'http://127.0.0.1:8000/api/get-system-pbsnode-info/' + node
      axios.get(url).then((response) =>{
        console.log(response)
        setNodeInfo(response.data)
      }).catch((error) =>{
        console.log(error.message)
      })
    } 

    const searchJob = () => {
      console.log(job)
      const url = 'http://127.0.0.1:8000/api/get-qstat-user-squeue/' + job
      axios.get(url).then((response) =>{
        console.log(response)
        setsaccount(response.data)
      }).catch((error) =>{
        console.log(error.message)
        setsaccount([])
      })
    } 
  




    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
      console.log(newValue)
      if (newValue == 2) {
        fetchSqueue()
      }
      setValue(newValue);
    };

    const navigate = useNavigate()

    
    
  useEffect(() => {
    axios.get('http://127.0.0.1:8000/api/admin/get-qstatinfo').then((response)=>{
      console.log(response.data);
      console.log(response)
    //   console.log(response.data.image_list[0]);
      setdata(response.data)
    }).catch((error)=>{
    //   console.log(error)
    })

  }, []);
  
  




      const handlePropChange = (newValue) => {
        // Change the prop value
        // console.log(newValue[0][0])
        setdata(newValue);
      };

  return (
    <div className='NodeDefinationTop'>
                    <div className='AdminDashboardTop'>

                <div className="AdminDashboardContentsTop container pt-5">

                <Box sx={{ width: '100%' }}>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs value={value} onChange={handleChange} textColor='#424242' variant="scrollable" scrollButtons="auto" aria-label="scrollable auto tabs example">
          <Tab label="Parttions" {...a11yProps(0)} />
          <Tab label="Node Search" {...a11yProps(1)} />
          <Tab label="queue" {...a11yProps(2)} />
          <Tab label="jobs" {...a11yProps(3)} />
          <Tab label="create" {...a11yProps(4)} />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
      <div className="AdminNodeDefinationTable">
                        {/* <NodeListTable rows={data}></NodeListTable> */}
                        <EnhancedTable grouplist={groups}  grouplistfunction={setgroups} rows={data} onButtonClick={handlePropChange}></EnhancedTable>
                    </div>
      </TabPanel>
      <TabPanel value={value} index={1}>
        
      <div className="">
                      <div className="">
                      {/* <button className="addNodeButton button-50" data-bs-toggle="modal" data-bs-target="#addSingleImage">
                            Add Image
                        </button> */}

                        <label htmlFor="nodename" className='me-2'>Node :</label>
                        <input type="text" name='nodename' id='nodename' value={node} onChange={(e)=>{
                          setNode(e.target.value)
                        }}/>
                        <button className='ms-2' onClick={searchNode}>
                          <span className="me-2">Search</span>
                        <i class="fa-solid fa-magnifying-glass"></i>
                        </button>

                      </div>

                    </div>

                    <div className="informationOfNode p-5">
                    {NodeInfo.length > 0 ? (
        <div className='text-start'>
                {Object.entries(NodeInfo[0]).map(([key, value]) => (
        <p key={key}>
          <b>{key} : </b> {value}
        </p>
      ))}
        </div>
      ) : (
        <p>
          {/* We currently don't have information on this image. */}
        </p>
      )}
                </div>

      </TabPanel>

      <TabPanel value={value} index={2}>
        
        
        <div className=''>
        <TableComponent columns={columns} rows={squeue}></TableComponent>
        </div>

      </TabPanel>

      <TabPanel value={value} index={3} oncl>
        
      <div className='workloadpageaccordian'>

      <div className="">
                      <div className="my-3">
                      {/* <button className="addNodeButton button-50" data-bs-toggle="modal" data-bs-target="#addSingleImage">
                            Add Image
                        </button> */}

                        <label htmlFor="jobname" className='me-2'>User :</label>
                        <input type="text" name='jobname' id='jobname' value={job} onChange={(e)=>{
                          setJob(e.target.value)
                        }}/>
                        <button className='ms-2' onClick={searchJob}>
                          <span className="me-2">Search</span>
                        <i class="fa-solid fa-magnifying-glass"></i>
                        </button>

                      </div>

                    </div>

          <TableComponent columns={columns} rows={saccount}></TableComponent>
        </div>

      </TabPanel>
      <TabPanel value={value} index={4} oncl>

        <BasicAccordion></BasicAccordion>

      </TabPanel>
    </Box>
                </div>


                

            </div>

<ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
    </div>
  )
}
