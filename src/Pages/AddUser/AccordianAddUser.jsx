import React from 'react'
import AccordianAddUserContent from './AccordianAddUserContent'
import SideBar from '../../Components/Sidebar/SideBar'

export default function AccordianAddUser() {
  return (
    <div>
        <SideBar childComponent={<AccordianAddUserContent />} ></SideBar>
    </div>
  )
}
