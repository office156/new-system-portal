import React from 'react'

export default function Partition() {
  return (
    <div>
        <div className="batchjobModal">
<button type="button" class="" data-bs-toggle="modal" data-bs-target="#partitionModal">
Partition
</button>

<div class="modal fade" id="partitionModal" tabindex="-1" aria-labelledby="partitionModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="partitionModalLabel">Partition</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <div className="formInputs">
        <label htmlFor="" className="form-label">Accounts Allowed</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Groups Allowed</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Qos Allowed</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Alternate</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">ClusterName</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Default</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Accounts Denied</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Qos denied</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Exclusive user</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Available features</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Active features</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Grace time</label>
        <input type="text" className="form-control" />
        <div className="formInputs">
        <label htmlFor="" className="form-label">Hidden</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Job Defaults</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Max CPUs per Node</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Node</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Nodelist</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Nodes Allowed Allocating</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Nodes Max</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Nodes (minimum)</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Oversubscribe</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">OverTimeLimit</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">State</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">PreempMode</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">PriorityJobFactor</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">PriorityTier</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Reason</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Root</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Time Limit</label>
        <input type="text" className="form-control" />
        </div>
        
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="" data-bs-dismiss="modal">Close</button>
        <button type="button" class="">Save changes</button>
      </div>
    </div>
  </div>
</div>
        </div>
    </div>
  )
}
