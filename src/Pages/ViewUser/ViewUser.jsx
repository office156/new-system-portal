import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { toast, ToastContainer } from 'react-toastify';
import "./ViewUser.css"


function splitArrayInHalf(arr) {
  const middleIndex = Math.floor(arr.length / 2);
  const firstHalf = arr.slice(0, middleIndex);
  const secondHalf = arr.slice(middleIndex);

  return [firstHalf, secondHalf];
}

export default function ViewUser() {
  const [userdata, setuserdata] = useState([])
  const [uid, setuid] = useState()
  const [firstHalf, secondHalf] = splitArrayInHalf(userdata);

  // const userdataList = userdata.map((fruit, index) => (
  //   <li key={index}>{fruit}</li>
  // ));

  const searchUser = () => {
    console.log(uid)
    const url = 'http://127.0.0.1:8000/api/get-user-full/' + uid
    axios.get(url).then((response) => {
      console.log(response.data);
      setuserdata(response.data)
      console.log(response.data.length)
      if (response.data.length == 0) {
        toast.error("User does not exist !")
      }
    }).catch((error) => {

    })
  }


  return (
    <div >
      <div className='text-center'>
        <label htmlFor="">User (UID) : </label>
        <input type="text" className='ms-2' onChange={(e) => {
          setuid(e.target.value)
        }} />
        <button onClick={searchUser} className='ms-2'>Search</button>
      </div>


      {userdata.length > 0 && <div className="userdata w-100">
        {/* {userdata[0].split(':')[1]} */}

        <h3 className='mt-5'>User Details :</h3>

        <div className="userdetails">
          <div className="nameUser row">
            <div className='col d-flex '>
              <div className="leftSide pe-3">First Name :</div>
              <div className="rightSide border-bottom">{userdata[1].split(':')[1]}</div>
            </div>

            <div className='col d-flex'>
              <div className="leftSide pe-3">Last Name :</div>
              <div className="rightSide border-bottom">{userdata[2].split(':')[1]}</div>
            </div>
          </div>

          <div className="userdetailsline">
            <div className="d-flex">
            <div className="leftSide pe-3">Organization name :</div>
            <div className="rightSide border-bottom">{userdata[21].split(':')[1]}</div>
            </div>

          </div>

          <div className="userdetailsline">
            <div className="d-flex">
            <div className="leftSide pe-3">Organization address :</div>
            <div className="rightSide border-bottom">{userdata[21].split(':')[1]}</div>
            </div>

          </div>

          <div className="userdetailsline">
            <div className="d-flex col-6">
            <div className="leftSide pe-3">Gender :</div>
            <div className="rightSide border-bottom">{userdata[19].split(':')[1]}</div>
            </div>

          </div>


          <div className="userdetailsline">
            <div className="d-flex col-6">
            <div className="leftSide pe-3">Department :</div>
            <div className="rightSide border-bottom">{userdata[14].split(':')[1]}</div>
            </div>

          </div>

          <div className="userdetailsline">
            <div className="d-flex col-6">
            <div className="leftSide pe-3">Designation :</div>
            <div className="rightSide border-bottom">{userdata[15].split(':')[1]}</div>
            </div>

          </div>

          <div className="nameUser row">
            <div className='col d-flex '>
              <div className="leftSide pe-3">*Roll No :</div>
              <div className="rightSide border-bottom">{userdata[1].split(':')[1]}</div>
            </div>

            <div className='col d-flex'>
              <div className="leftSide pe-3">*Course :</div>
              <div className="rightSide border-bottom">{userdata[2].split(':')[1]}</div>
            </div>
          </div>

          <div className="userdetailsline">
            <div className="d-flex col-6">
            <div className="leftSide pe-3">Official email address :</div>
            <div className="rightSide border-bottom">{userdata[7].split(':')[1]}</div>
            </div>

          </div>

          <div className="nameUser row">
            <div className='col d-flex '>
              <div className="leftSide pe-3">*Office No :</div>
              <div className="rightSide border-bottom">{userdata[1].split(':')[1]}</div>
            </div>

            <div className='col d-flex'>
              <div className="leftSide pe-3">*Mobile No :</div>
              <div className="rightSide border-bottom">{userdata[2].split(':')[1]}</div>
            </div>
          </div>


          <div className="userdetailsline">
            <div className="d-flex">
            <div className="leftSide pe-3">*Nature of research :</div>
            <div className="rightSide border-bottom">{userdata[7].split(':')[1]}</div>
            </div>

          </div>

          <hr className='my-5'/>
<h3 className='mb-3'>Project Details</h3>

<div className="userdetailsline">
            <div className="d-flex">
            <div className="leftSide pe-3">Project Name :</div>
            <div className="rightSide border-bottom">{userdata[23].split(':')[1]}</div>
            </div>

          </div>

<div className="userdetailsline">
            <div className="d-flex">
            <div className="leftSide pe-3">*Nature of Project :</div>
            <div className="rightSide border-bottom">{userdata[23].split(':')[1]}</div>
            </div>

          </div>

<div className="userdetailsline">
            <div className="d-flex">
            <div className="leftSide pe-3">*Brief description of project :</div>
            <div className="rightSide border-bottom">{userdata[23].split(':')[1]}</div>
            </div>

          </div>



        </div>



        {/* <div className="userdetailsline d-flex">
          <div className='d-flex'>
          <div className="leftSide">First Name :</div>
          <div className="rightSide">{userdata[1].split(':')[1]}</div>
          </div>

          <div className='d-flex'>
          <div className="leftSide">Last Name :</div>
          <div className="rightSide">{userdata[2].split(':')[1]}</div>
          </div>
        </div>
        <div className="userdetailsline">
          <span className="leftSide">Gender :</span>
        </div> */}
        {/*  */}
        {/* <div className="userdetailsline">
          <span className="leftSide">First Name :</span>
          <span className="rightSide">{userdata[0].split(':')[1]}</span>
        </div>
        <div className="userdetailsline">
          <span className="leftSide">First Name :</span>
          <span className="rightSide">{userdata[0].split(':')[1]}</span>
        </div> */}
      </div>
      }

      {/* <div className='container d-flex mt-4 justify-content-center'  >
      <div className="half">
        <ul>
          {firstHalf.map((item, index) => (
            <li key={index}>{item}</li>
          ))}
        </ul>
      </div>
      <div className="half">
        <ul>
          {secondHalf.map((item, index) => (
            <li key={index}>{item}</li>
          ))}
        </ul>
      </div>
    </div> */}

      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="dark"
      />
    </div>
  )
}
