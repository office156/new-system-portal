import React from 'react'
import NodeDefinationContent from './NodeDefinationContent'
import SideBar from '../../Components/Sidebar/SideBar'

export default function NodeDefination() {
  return (
    <div>
        <SideBar childComponent={<NodeDefinationContent />} ></SideBar>
    </div>
  )
}
