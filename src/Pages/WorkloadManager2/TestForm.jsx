import React, { useState } from 'react';

const MyForm = () => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    message: '',
    radioValue: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formData)
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" name="name" onChange={handleChange} />
      <input type="email" name="email" onChange={handleChange} />
      <textarea name="message" onChange={handleChange} />
      <label>
        <input
          type="radio"
          name="radioValue"
          value="option1"
          onChange={handleChange}
          checked={formData.radioValue === 'option1'}
        />
        Option 1
      </label>
      <label>
        <input
          type="radio"
          name="radioValue"
          value="option2"
          onChange={handleChange}
          checked={formData.radioValue === 'option2'}
        />
        Option 2
      </label>
      <button type="submit">Submit</button>
    </form>
  );
};

export default MyForm;
