import React from 'react'
import Network from './Network'
import SideBar from '../../Components/Sidebar/SideBar'

export default function NetworkPage() {
  return (
    <div>
        <SideBar childComponent={<Network />} ></SideBar>
    </div>
  )
}
