import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import CreateForm from './CreateForm';
import axios from 'axios';
import { useRef } from 'react';
import { useState } from 'react';
import MyForm from './TestForm';



export default function BasicAccordion() {

  const fileInputRef = useRef(null);
  const [selectedFile, setSelectedFile] = useState(null);


  const handleFileChange = (event) => {
    const file = event.target.files[0];
    if (file && file.name.endsWith('.batch')) {
      setSelectedFile(file);
    } else {
      setSelectedFile(null);
      alert('Please select a .batch file.');
    }
  };

  const handleSubmit = () => {
    if (selectedFile) {
      const formData = new FormData();
      formData.append('batch_file', selectedFile);

      // You can add a loading state here if you want to show a loading indicator while the file is uploading
      // setLoading(true);

      axios.post('http://127.0.0.1:8000/api/sbatch-file', formData)
        .then((response) => {
          // Handle the response from the backend if needed
          console.log(response.data);

          // Clear the file input after successful upload
          fileInputRef.current.value = '';

          // You can reset the loading state here if you have set it
          // setLoading(false);
        })
        .catch((error) => {
          // Handle error if the upload fails
          console.error('Error uploading file:', error);

          // You can reset the loading state here if you have set it
          // setLoading(false);
        });
    } else {
      alert('Please select a .batch file.');
    }
  };



  return (
    <div>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Create Queue</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            <CreateForm></CreateForm>
            {/* <MyForm></MyForm> */}
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>Create batch job</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            <label htmlFor="script">Script :</label>
            <input type="file" name="script" id="script"  ref={fileInputRef}  onChange={handleFileChange} />
            <button className="btn btn-primary" onClick={handleSubmit}>Submit</button>
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}