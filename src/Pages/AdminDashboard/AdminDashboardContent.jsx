import React from 'react'
// import "./AdminDashboard.css"
// import Sidebar from '../../Components/Sidebar_backup/Sidebar'
import DashboardCards from '../../Components/DashboardCards/DashboardCards'
import SimpleTable from '../../Components/TemplateSimpleTable/SimpleTable'
import SimpleAreaChart from '../../Components/TemplateCharts/AreaChart/AreaChart'
import SimpleBarChart from '../../Components/TemplateCharts/BarChart/SimpleBarChart'

export default function AdminDashboardContent() {
  return (

        <div className="AdminDashboardContentsTop pt-5">
          <DashboardCards></DashboardCards>

          {/* <div className="AdminChartMiddle d-flex justify-content-around py-5">
          <div className="AdminChartsLeft mx-2">
            <DBLineChart></DBLineChart>
          </div>
          <div className="AdminChartsRight mx-2">
            <DBAreaChart></DBAreaChart>
          </div>
        </div>

        <div className="AdminDashboardUserTable mx-2">
          <DBUserTable></DBUserTable>
        </div> */}

        <div className="AdminDashboardCharts row gap-3 my-5 align-item-center">
          {/* <SimpleAreaChart></SimpleAreaChart> */}

          <div className="col">
          <SimpleAreaChart></SimpleAreaChart>
          </div>

          <div className="col">
            <SimpleBarChart></SimpleBarChart>
          </div>


        </div>

        <div className="adminDashBoardtable mt-5">
        <SimpleTable></SimpleTable>
        </div>

        </div>

  )
}
