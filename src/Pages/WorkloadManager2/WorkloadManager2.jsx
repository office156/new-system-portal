import React from 'react'
import WorkloadManagerContent from './WorkloadManager2Content'
import SideBar from '../../Components/Sidebar/SideBar'

export default function WorkloadManager2() {
  return (
    <div>
        <SideBar childComponent={<WorkloadManagerContent />} ></SideBar>
    </div>
  )
}
