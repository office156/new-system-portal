import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { toast , ToastContainer } from 'react-toastify';




export default function DeleteUserContent() {
  const [userdata , setuserdata] = useState([])
  const [uid , setuid] = useState()
  const [firstHalf, setFirstHalf] = useState([]);
  const [secondHalf, setSecondHalf] = useState([]);

  // const userdataList = userdata.map((fruit, index) => (
  //   <li key={index}>{fruit}</li>
  // ));

  const handleButtonClick = () => {
    const userConfirmed = window.confirm('Are you sure you want to perform this action?');
    
    if (userConfirmed) {
      // Put your code to execute when the user confirms here
      console.log('Action confirmed!');
    } else {
      // Put your code to execute when the user cancels here
      console.log('Action canceled.');
    }
  };

  const searchUser = () =>{
    // console.log(uid)
    const url = 'http://127.0.0.1:8000/api/get-user-full/' + uid
    axios.get(url).then((response)=>{
      console.log(response.data);
      // setuserdata(response.data)
      if (response.data.length == 0) {
        toast.error("User does not exist !")
      }else{
        const midpoint = Math.ceil(response.data.length / 2);
        const firstHalf = response.data.slice(0, midpoint);
        const secondHalf = response.data.slice(midpoint);
    
        setFirstHalf(firstHalf);
        setSecondHalf(secondHalf);
      }


    }).catch((error)=>{
      console.log(error)
    })
  }

  const deleteUser = () =>{
    console.log(uid)
    const url = 'http://127.0.0.1:8000/api/delete-user-full/' + uid
    axios.get(url).then((response)=>{
      console.log(response.data);
      toast.success("User deleted successfully")
      setFirstHalf([])
      setSecondHalf([])
    }).catch((error)=>{
      console.log(error)
    })
  }


  return (
    <div >
      <div className='text-center'>
      <label htmlFor="">User (UID) : </label>
      <input type="text" className='ms-2' onChange={(e)=>{
        setuid(e.target.value)
      }} />
      <button onClick={searchUser} className='ms-2'>Search</button>
      <button onClick={handleButtonClick} className='ms-2'>Delete</button>
      </div>


      {/* <div className="userdata text-center w-100">
        {userdataList}
      </div> */}
        <div className='container d-flex mt-4 justify-content-center'  >
      <div className="half">
        <ul>
          {firstHalf.length > 0 && firstHalf.map((item, index) => (
            <li key={index}>{item}</li>
          ))}
        </ul>
      </div>
      <div className="half">
        <ul>
          {secondHalf.length > 0 && secondHalf.map((item, index) => (
            <li key={index}>{item}</li>
          ))}
        </ul>
      </div>
    </div>

      <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
    </div>
  )
}
