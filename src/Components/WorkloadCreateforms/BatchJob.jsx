import React from 'react'

export default function BatchJob() {
  return (
    <div>
      <div className="batchjobModal">
<button type="button" class="" data-bs-toggle="modal" data-bs-target="#BatchJobModal">
Batch job
</button>

<div class="modal fade" id="BatchJobModal" tabindex="-1" aria-labelledby="BatchJobModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="BatchJobModalLabel">Batch job</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <div className="formInputs mb-3 text-start">
        <label htmlFor="" className="form-label">Script file</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs mb-3 text-start">
        <label htmlFor="" className="form-label">Time limit</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs mb-3 text-start">
        <label htmlFor="" className="form-label">Nodes (Minimum)</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs mb-3 text-start">
        <label htmlFor="" className="form-label">Task count</label>
        <input type="text" className="form-control" />
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="" data-bs-dismiss="modal">Close</button>
        <button type="button" class="">Save changes</button>
      </div>
    </div>
  </div>
</div>
        </div>
    </div>
  )
}
