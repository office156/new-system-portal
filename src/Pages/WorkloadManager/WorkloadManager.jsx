import React from 'react'
import WorkloadManagerContent from './WorkloadManagerContent'
import SideBar from '../../Components/Sidebar/SideBar'

export default function WorkloadManager() {
  return (
    <div>
        <SideBar childComponent={<WorkloadManagerContent />} ></SideBar>
    </div>
  )
}
