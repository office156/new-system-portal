import React, { useState } from 'react'
import axios from 'axios';
import TableComponent from '../../Components/TableComponent/TableComponent';
import { useEffect } from 'react';
import "./Network.css"

export default function Network() {

  const [data , setData] = useState([])
  const [rows , setrows] = useState([]);

  const columns = [
    { field: 'netname',minWidth :250, headerName: 'netname' },
    { field: 'net',minWidth :220, headerName: 'net' },
    { field: 'mask',minWidth :220, headerName: 'mask' },
    { field: 'mgtifname',minWidth :220, headerName: 'mgtifname' },
    { field: 'gateway',minWidth :220, headerName: 'gateway' },
    // { field: 'dhcpserver', headerName: 'dhcpserver' },
    { field: 'tftpserver',minWidth :220, headerName: 'tftpserver' },
    // { field: 'nameservers', headerName: 'nameservers' },
    // { field: 'ntpservers', headerName: 'ntpservers' },
    // { field: 'logservers', headerName: 'logservers' },
    // { field: 'dynamicrange', headerName: 'dynamicrange' },
    // { field: 'staticrange', headerName: 'staticrange' },
    // { field: 'staticrangeincrement', headerName: 'staticrangeincrement' },
    // { field: 'nodehostname', headerName: 'nodehostname' },
    // { field: 'ddnsdomain', headerName: 'ddnsdomain' },
    // { field: 'vlanid', headerName: 'vlanid' },
    // { field: 'domain', headerName: 'domain' },
    { field: 'mtu', headerName: 'mtu' },
    // { field: 'comments', headerName: 'comments' },
    // { field: 'disable', headerName: 'disable' },
  ];

  const getData = () => {
    if (data.length === 0) {
      axios.get('http://127.0.0.1:8000/api/admin/get-network-info')
      .then(response => {
        console.log(response)
        setData(response.data)
      })
      .catch(error => {
        // Handle the error
        console.log(error);
      });
    } else {
      setData([])
}
  }

  useEffect(() => {
    axios.get('http://127.0.0.1:8000/api/get-tabdump').then((response)=>{
      console.log(response.data);
      console.log(response)
      setrows(response.data)
    }).catch((error)=>{
      console.log(error)
    })
  }, [])
    

  return (
    <div>

<div className=''>
        <TableComponent columns={columns} rows={rows}></TableComponent>
        </div>




      <button className='button-30 mt-3' onClick={getData}>NMCLI</button>

      <div className="ms-5 mt-5 border p-4">
      {data.map((paragraph, index) => (
          <div key={index} style={{ whiteSpace: 'pre' }} className='mb-3'>
            {paragraph.map((line, lineIndex) => (
              <div key={lineIndex} style={{ whiteSpace: 'pre' }}>{line}</div>
            ))}
          </div>
        ))}
      </div>

    </div>
  )
}
