import React from 'react'
import SideBar from '../../Components/Sidebar/SideBar'
import ViewUser from './ViewUser'

export default function ViewUserMain() {
  return (
    <div>
        <SideBar childComponent={<ViewUser />} ></SideBar>
    </div>
  )
}
