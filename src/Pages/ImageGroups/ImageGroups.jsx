import React from 'react'
import ImageGroupsContent from './ImageGroupsContent'
import SideBar from '../../Components/Sidebar/SideBar'

export default function ImageGroups() {
  return (
    <div>
        <SideBar childComponent={<ImageGroupsContent />} ></SideBar>
    </div>
  )
}
