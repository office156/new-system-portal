import React from 'react'
import "./Add.css"
import { useState } from 'react';
import { ToastContainer , toast } from 'react-toastify';
import axios from 'axios';

export default function AccordianAddUserContent() {


  const [formData, setFormData] = useState({
mail: '',
Displayname:'',
Loginshell:'',
firstname:'',
lastname:'',
Homedirectory:'',
Subdomain:'',
Startdate:'',
Projectname:'',
PIHOD:'',
Institute:'',
GPUhours:'',
Gender:'male',
Funded:'paid',
Enddate:'',
Domain:'',
Designation:'',
Department:'',
CPUhours: '',
Application: '',
Amount: '',
mobile: '',
Postalcode: '',
ST: '',
Street: '',
city: '',

  });


  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  const clearPage = (event) => {
    setFormData({
      mail: '',
      Displayname:'',
      Loginshell:'',
      firstname:'',
      lastname:'',
      Homedirectory:'',
      Subdomain:'',
      Startdate:'',
      Projectname:'',
      PIHOD:'',
      Institute:'',
      GPUhours:'',
      Gender:'male',
      Funded:'paid',
      Enddate:'',
      Domain:'',
      Designation:'',
      Department:'',
      CPUhours: '',
      Application: '',
      Amount: '',
      mobile: '',
      Postalcode: '',
      ST: '',
      Street: '',
      city: '',
    });
  };
  

  const AddUser = (event) => {
    event.preventDefault();

    if (formData.UID === '' || formData.Displayname ==='' || formData.Loginshell ==='' || formData.UIDnumber ==='' || formData.GIDnumber ==='' || formData.Homedirectory ==='' || formData.Subdomain ==='' || formData.Startdate ==='' || formData.Projectname ==='' || formData.PIHOD ==='' || formData.Institute ==='' || formData.GPUhours ==='' || formData.Gender ==='' || formData.Funded ==='' || formData.Enddate ==='' || formData.Domain ==='' || formData.Designation ==='' || formData.Department ==='' || formData.CPUhours === '' || formData.Application === '' || formData.Amount === '' || formData.mobile === '' || formData.Postalcode === '' || formData.ST === '' || formData.Street === '' || formData.l === '') {
        console.log('Please fill all input fields.');
        console.log(formData)
      toast.error("Please enter all the fields")


    } else {
              console.log('All input fields are filled.');
                console.log(formData);
                try {
                  const response = axios.post('http://127.0.0.1:8000/api/admin/user-defination', {
                    formData
                  });
                } catch (error) {
                  console.error(error);
                  toast.error(error)
                }

                setFormData({
                  mail: '',
                  Displayname:'',
                  Loginshell:'',
                  firstname:'',
                  lastname:'',
                  Homedirectory:'',
                  Subdomain:'',
                  Startdate:'',
                  Projectname:'',
                  PIHOD:'',
                  Institute:'',
                  GPUhours:'',
                  Gender:'male',
                  Funded:'paid',
                  Enddate:'',
                  Domain:'',
                  Designation:'',
                  Department:'',
                  CPUhours: '',
                  Application: '',
                  Amount: '',
                  mobile: '',
                  Postalcode: '',
                  ST: '',
                  Street: '',
                  city: '',                  
                });

                toast.success("User added successfully");
    }
    
  }

 
 
  return (
    <div className='container mt-5'>
      <form onSubmit={AddUser}>
        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor="">Mail : </label>
        <input type="email" className='w-100 accordain-input' placeholder='Mail' name="mail" value={formData.mail} onChange={handleInputChange} />
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Displayname" value={formData.Displayname} onChange={handleInputChange} placeholder='Display name' />
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Loginshell" value={formData.Loginshell} onChange={handleInputChange} placeholder='Login shell'/>
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="firstname" value={formData.firstname} onChange={handleInputChange} placeholder='First name' />
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="lastname" value={formData.lastname} onChange={handleInputChange} placeholder='Last name'/>
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Homedirectory" value={formData.Homedirectory} onChange={handleInputChange} placeholder='Home directory' />
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Subdomain" value={formData.Subdomain} onChange={handleInputChange} placeholder='Sub domain'/>
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="date" className='w-100 accordain-input' name="Startdate" value={formData.Startdate} onChange={handleInputChange} placeholder='Start date' />
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Projectname" value={formData.Projectname} onChange={handleInputChange} placeholder='Project name'/>
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="PIHOD" value={formData.PIHOD} onChange={handleInputChange} placeholder='PI-HOD' />
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Institute" value={formData.Institute} onChange={handleInputChange} placeholder='Institute'/>
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="number" className='w-100 accordain-input' name="GPUhours" value={formData.GPUhours} onChange={handleInputChange} placeholder='GPU hours' />
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        {/* <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Gender" value={formData.Gender} onChange={handleInputChange} placeholder='Gender'/>
         */}

<label className='styleLabels' for="mygenderDropdown">Gender : </label>
        <select id="mygenderDropdown" value={formData.Gender} onChange={handleInputChange} name="Gender">
            <option value="male">Male</option>
            <option value="female">Female</option>
        </select>
        </div>

        <div className='inputFields col'>
        {/* <label htmlFor=""></label> */}
        <label className='styleLabels' for="myfundedDropdown">Funded : </label>
        <select id="myfundedDropdown" value={formData.Funded} onChange={handleInputChange} name="Funded">
            <option value="paid">paid</option>
            <option value="default">default</option>
        </select>

        
        {/* <input type="text" className='w-100 accordain-input' name="Funded" value={formData.Funded} onChange={handleInputChange} placeholder='Funded' /> */}
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="date" className='w-100 accordain-input' name="Enddate" value={formData.Enddate} onChange={handleInputChange} placeholder='End date'/>
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Domain" value={formData.Domain} onChange={handleInputChange} placeholder='Domain' />
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Designation" value={formData.Designation} onChange={handleInputChange} placeholder='Designation'/>
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Department" value={formData.Department} onChange={handleInputChange} placeholder='Department' />
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="number" className='w-100 accordain-input' name="CPUhours" value={formData.CPUhours} onChange={handleInputChange} placeholder='CPU hours'/>
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Application" value={formData.Application} onChange={handleInputChange} placeholder='Application' />
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="number" className='w-100 accordain-input' name="Amount" value={formData.Amount} onChange={handleInputChange} placeholder='Amount'/>
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="number" className='w-100 accordain-input' name="mobile" value={formData.mobile} onChange={handleInputChange} placeholder='mobile' />
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="number" minLength="6" maxLength="6" className='w-100 accordain-input' name="Postalcode" value={formData.Postalcode} onChange={handleInputChange} placeholder='Postal code'/>
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="ST" value={formData.ST} onChange={handleInputChange} placeholder='ST:' />
        </div>
        </div>

        <div className="row mt-2">
        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="Street" value={formData.Street} onChange={handleInputChange} placeholder='Street'/>
        </div>

        <div className='inputFields col'>
        <label htmlFor=""></label>
        <input type="text" className='w-100 accordain-input' name="city" value={formData.city} onChange={handleInputChange} placeholder='City' />
        </div>
        </div>
        

        <button type="submit" className='mt-5 button-accordian-user'>Submit</button>
        <button className='mt-3 button-accordian-user' onClick={clearPage}>Clear</button>

        </form>

        <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>


    </div>
  )
}

