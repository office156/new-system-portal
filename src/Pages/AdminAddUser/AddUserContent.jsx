import React from 'react'
import Sidebar from '../../Components/Sidebar_backup/Sidebar'
import "./AddUser.css"
import AddUserTable from '../../Components/AddUserTable/AddUserTable'
import MultiPageUserForm from '../../Components/MultiPageUserForm/MultiPageUserForm'
import { useState } from 'react'
import TableComponent from '../../Components/TableComponent/TableComponent'
import { useEffect } from 'react'
import axios from 'axios'
import SideBar from '../../Components/Sidebar/SideBar'
import CollapsibleTable from '../../Components/CollapsibleTable/CollapsibleTable'


export default function AddUserContent() {

  const [isToggleOn, setIsToggleOn] = useState(false);
  const [userlistrow, setuserlistrow] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  // const [searchResults, setSearchResults] = useState([]);


  const userlistcolumns = [
    { field: 'uidNumber',minWidth : 150, headerName: 'Uid Number' },
    { field: 'gidNumber',minWidth : 150, headerName: 'Gid Number' },
    {
      field: 'loginShell',minWidth : 150,
      headerName: 'Login Shell'
    },
    { field: 'homeDirectory',minWidth : 150, headerName: 'Home Directory' },
    { field: 'uid',minWidth : 150, headerName: 'UID' },
    { field: 'displayName',minWidth : 150, headerName: 'Display Name' },
    { field: 'moreInfo',minWidth : 150, headerName: 'More info' },
  ];

  const hideForm = () => {
    setIsToggleOn(!isToggleOn);
  };



  useEffect(() => {
    axios.get('http://127.0.0.1:8000/api/get-userlist').then((response)=>{
      console.log(response.data);
      setuserlistrow(response.data)
    }).catch((error)=>{

    })
  }, []);

  const handleSearch = async () => {
    // const filteredResults = data.filter(item =>
    //   item.name.toLowerCase().includes(searchTerm.toLowerCase())
    // );
    // setSearchResults(filteredResults);
    if (searchTerm.toLowerCase() === '') {
      await axios.get('http://127.0.0.1:8000/api/get-userlist').then((response)=>{
        console.log(response.data);
        setuserlistrow(response.data)
      }).catch((error)=>{
  
      })
    }else{
      const url = 'http://127.0.0.1:8000/api/get-userlist/' + searchTerm.toLowerCase()
      await axios.get(url).then((response)=>{
        console.log(response.data);
        setuserlistrow(response.data)
      }).catch((error)=>{
  
      })
    }

  };


  return (
    <div className='AdminDashboardTop'>

        <div className="topcontentadduser">
        <div className="AddUserRightTop">
              <div className="AddUserRightTopText d-flex justify-content-between px-2 py-2 align-item-center">
                <span>
                  User
                </span>
                <i class="fa-solid fa-up-right-and-down-left-from-center"></i>
              </div>

              <hr className='p-0 m-0' />

              <div className="AddUserSearch py-2 d-flex justify-content-between px-3">
                

                <div className="AddUserSearchBar border-bottom d-flex justify-content-between mx-3 w-100">
                  <div>
                  <i class="fa-solid fa-magnifying-glass"></i>
                  <input className='border-0 class ms-2' type="text" name="" id="" placeholder='Search settings inputs'         value={searchTerm}
        onInput={(e) => {
          setSearchTerm(e.target.value);
          handleSearch();
        }} />
                  
                  </div>
                  <i class="fa-solid fa-xmark"></i>
                </div>

                {/* <i class="fa-solid fa-chevron-up"></i> */}

                
              </div>

            </div>
        </div>
        
        <div className="middlecontentadduser">
        <div className="AdminAddUserTable mb-2">
            
            <CollapsibleTable columns={userlistcolumns} rows={userlistrow}></CollapsibleTable>
            {/* <TableComponent columns={userlistcolumns} rows={userlistrow}></TableComponent> */}
          </div>
          <div className="AdminAddUserButton py-3">
            <button className='AddUserButton me-3 specialButton' onClick={hideForm}>
            <i class="fa-solid fa-plus"></i>
            <span className='ms-2'>ADD</span>
            </button>
          </div>
        </div>
        <div className="AddUserRightMiddle px-3 py-3 text-start">
              <span className="AddMiddleText">
                JUMP TO
              </span>
              <div className="AddMiddleButtonDiv my-3">
                <button className="AddMiddleButton specialButton">
                  <span className='me-2'>Project manager</span>
                <i class="fa-solid fa-chevron-right"></i>
                </button>
                <button className='ms-1 specialButton'>
                <i class="fa-solid fa-chevron-down"></i>
                </button>
              </div>
            </div>
            <div className="AddUserRightBottom">
            {isToggleOn ? <MultiPageUserForm></MultiPageUserForm> : <></>}
              
            </div>
          

    </div>
  )
}
