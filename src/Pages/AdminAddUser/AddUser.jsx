import React from 'react'
import AddUserContent from './AddUserContent'
import SideBar from '../../Components/Sidebar/SideBar'

export default function AddUser() {
  return (
    <div>
        <SideBar childComponent={<AddUserContent />} ></SideBar>
    </div>
  )
}
