import React from 'react'
import { useState, useRef } from 'react';
import axios from 'axios';
import { ToastContainer , toast } from 'react-toastify';
import { useEffect } from 'react';

export default function UpdateNodeModal({setData,image}) {
    
  const [file1, setFile1] = useState(null);
  const [file2, setFile2] = useState(null);
  const fileInputRef1 = useRef(null);
  const fileInputRef2 = useRef(null);

  const handleFile1Change = (event) => {
    setFile1(event.target.files[0]);
  };

  const handleFile2Change = (event) => {
    setFile2(event.target.files[0]);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    const formData = new FormData();
    formData.append('file1', file1);
    formData.append('file2', file2);

    axios.post('http://127.0.0.1:8000/api/admin/update-image', formData)
      .then((response) => {
        // Handle response
        console.log(response)
        // Reset form
        setFile1(null);
        setFile2(null);
        fileInputRef1.current.value = null;
        fileInputRef2.current.value = null;
      })
      .catch((error) => {
        // Handle error
      });
  };
    
      // const handleSubmitNode = async (event) => {
      //   event.preventDefault();
      //   if (true) {
      //     // some input fields are empty, show an error message
      //       console.log('Please fill all input fields.');
      //     toast.error("Please enter all the fields")
  
  
      //   } else {
  
      //             console.log('All input fields are filled.');
      //               try {
      //                 const response = await axios.post('http://127.0.0.1:8000/api/admin/update-node-defination');
      //                 console.log(response);
      //               } catch (error) {
      //                 console.error(error);
      //                 toast.error(error)
      //               }

  
      //               toast.success("Node updated successfully");
      //   }
    
      //   axios.get('http://127.0.0.1:8000/api/test').then((response)=>{
      //     console.log(response.data);
      //     setData(response.data)
      //   }).catch((error)=>{
      //     console.log(error)
      //   })
        
  
      // };

  return (
    
    <>
  <div>
  <div class="modal-body">
      <form>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Exlist</label> 
    <input type="file" class="form-control"  name="Exlist" onChange={handleFile1Change} ref={fileInputRef1}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Pkglist</label> 
    <input type="file" class="form-control"  name="Pkglist" onChange={handleFile2Change} ref={fileInputRef2}></input>
 
  </div>

</form>


      </div>
      <div class="modal-footer">
        <button type="button" class="NodeDefinationButtons button-55" data-bs-dismiss="modal">Close</button>
        <button type="button" class="NodeDefinationButtons button-55" onClick={handleSubmit}>Save changes</button>
      </div>
      </div>

</>


  )
}
